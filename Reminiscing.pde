/**
 * Reminiscing.
 * A creative coding animation that draws Ouroboros loop.
 * 
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.03.09
 */


void setup() {

  size(720, 720);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  smooth();
  noLoop();

}

void draw() {

  int   frmMax     = 24 * 6; // 24fps x 6s animation
  int   scaleMax   = 16;
  int   spineMax   = 32;
  float moveRadius = min(width, height) * random(0.30, 0.35);
  float bodyRadius = min(width, height) * random(0.1, 0.2);
  float hueBase    = random(360.0);

  
  translate(width * 0.5, height * 0.5);
  rotate(random(PI));

  for (int frmCnt = 0; frmCnt < frmMax; ++frmCnt) {

    float frmRatio = map(frmCnt, 0, frmMax, 0.0, 1.0);

    background(0.0, 0.0, 90.0, 100.0);
    ArrayList<PVector> currPvs = new ArrayList();
    ArrayList<PVector> prevPvs = new ArrayList();
    for (int spineCnt = 0; spineCnt < spineMax; ++spineCnt) {
   
      float spineRatio = map(spineCnt, 0, spineMax, 0.0, 1.0);
      float moveRatio  = (frmRatio + spineRatio) % 1.0;
      float spineX = moveRadius * cos(TWO_PI * moveRatio);
      float spineY = moveRadius * sin(TWO_PI * moveRatio);

      // calculate current spine
      currPvs.clear();
      for (int scaleCnt = 0; scaleCnt < scaleMax; ++scaleCnt) {
        float pRadian = TWO_PI * (scaleCnt * 1.0 / scaleMax + map(sin(TWO_PI * (scaleCnt * 1.0 / scaleMax + moveRatio)), -1.0, 1.0, -0.05, 0.05));
        float pRadius = bodyRadius * (0.5 + 0.6 * spineRatio) * map(sin(TWO_PI * (scaleCnt * 1.0 / scaleMax + moveRatio)), -1.0, 1.0, 0.6, 1.05);
        float pX = pRadius * cos(pRadian) + spineX;
        float pY = pRadius * sin(pRadian) + spineY;
        currPvs.add(new PVector(pX, pY));
      }
      
      if (prevPvs.size() != 0) {
        drawSpine(prevPvs, currPvs, hueBase, spineRatio);
      }

      // set previous spine
      prevPvs.clear();
      for (PVector pv : currPvs) {
        prevPvs.add(pv);
      }

    }
    saveFrame("frames/" + String.format("%04d", frmCnt) + ".png");
  }
  exit();
}

/**
 * drawSpine draw Ouroboros spine.
 * @param  _prevPvs    : coordinates of previous spine.
 * @param  _currPvs    : coordinates of current spine.
 * @param  _baseHue    : body color.
 * @param  _spineRatio : 0.0 - 1.0 which spine to draw.
 */
private void drawSpine(ArrayList<PVector> _prevPvs, ArrayList<PVector> _currPvs, float _baseHue, float _spineRatio) {

  float divHue = 0.0;

  strokeWeight(_spineRatio * 2.0);
  stroke(0.0, 0.0, 90.0, 100.0);
  beginShape(TRIANGLE_FAN);
  for (int i = 0; i < _currPvs.size(); i++) {
    vertex(_prevPvs.get(i).x, _prevPvs.get(i).y);
    fill(
         (_baseHue + divHue + _spineRatio * 60.0) % 360.0,
         40.0 + 20.0 * _spineRatio,
         40.0 + 20.0 * _spineRatio,
         100.0
         );
    vertex(_currPvs.get(i).x, _currPvs.get(i).y);
    divHue += 5.0;
  }
  endShape(CLOSE);

}
